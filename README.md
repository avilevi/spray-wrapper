Simple spray client wrapper 

=========================

Allows requests without defining a pipeline 
 like :

```
#!scala

 PostRequest(postUri,Some(content))
      .addContentType(ContentTypes.`application/json`)
      .mapTo[SampleResponse]
```
 and 

```
#!scala

   GetRequest(uri)
      .addHeaders(BasicHeader(Authorization.name,"anythingYouWant"))
      .addHeaders(BasicCredential("some_key","some_value"))
      .addContentType(ContentTypes.`application/json`)
      .mapTo[SampleGetResponse]
```



For usage checkout the Main class example