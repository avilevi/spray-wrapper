package com.example

import akka.actor.ActorSystem
import akka.event.Logging
import akka.io.IO
import akka.pattern.ask
import avi.httpx.HttpClientWrapper
import spray.can.Http
import spray.http.HttpHeaders.Authorization
import spray.http._
import spray.json._
import spray.util._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.util.{Failure, Success}

case class Elevation(location: Location, elevation: Double, resolution:Double)
case class Location(lat: Double, lng: Double)
case class GoogleApiResult[T](status: String, results: List[T])

object ElevationJsonProtocol extends DefaultJsonProtocol {
  implicit val locationFormat = jsonFormat2(Location)
  implicit val elevationFormat = jsonFormat3(Elevation)
  implicit def googleApiResultFormat[T :JsonFormat] = jsonFormat2(GoogleApiResult.apply[T])
}

case class SampleBody(title:String, body:String, userId:Int)
case class SampleData(data:SampleBody)
case class SampleResponse(data:SampleBody,id:Int)
case class SampleGetResponse(userId:Int,id:Int,title:String,body:String)

object SampleDataJsonProtocol extends DefaultJsonProtocol{
  implicit val bodyFormat = jsonFormat3(SampleBody)
  implicit val dataFormat = jsonFormat1(SampleData)
  implicit val sampleResponse = jsonFormat2(SampleResponse)
  implicit val sampleGetResponse = jsonFormat4(SampleGetResponse)


}

object Main extends App with HttpClientWrapper
{
  implicit val system = ActorSystem("simple-spray-client")
  // we need an ActorSystem to host our application in
  import ElevationJsonProtocol._
  import SampleDataJsonProtocol._
  val log = Logging(system, getClass)

  log.info("Requesting the elevation of Mt. Everest from Googles Elevation API...")

  //  val uri = "http://maps.googleapis.com/maps/api/elevation/json?locations=27.988056,86.925278&sensor=false"
  val uri = "http://jsonplaceholder.typicode.com/posts/1"
  val postUri = "http://jsonplaceholder.typicode.com/posts"

  val postResponse =  {
    val content = SampleData(SampleBody("foo","bar",1)).toJson.toString()
    PostRequest(postUri,Some(content))
      .addContentType(ContentTypes.`application/json`)
      .mapTo[SampleResponse]
  }

  postResponse onComplete  {
    case Success(response) =>
      log.warning(s"Successful POST response: $response")

    case Failure(error) =>
      log.error(error, "Couldn't get post response")
  }

  val res =  {
    GetRequest(uri)
      .addHeaders(BasicHeader(Authorization.name,"anythingYouWant"))
      .mapTo[SampleGetResponse]
  }
  res onComplete {
    case Success(response) =>
      log.warning(s"Successful GET response: $response")

    case Failure(error) =>
      log.error(error, "Couldn't get")
  }

  for {
    r <- res
    r2 <- postResponse
    r3 <-  DeleteRequest(uri).get
    val x = log.info(s" Delete request responded with ${r3.status}")
  }yield shutdown()


  def shutdown(): Unit = {
    IO(Http).ask(Http.CloseAll)(1.second).await
    system.shutdown()
  }
}