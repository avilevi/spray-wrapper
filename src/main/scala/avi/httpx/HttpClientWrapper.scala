package avi.httpx

import akka.actor.ActorSystem
import akka.event.{LoggingAdapter, Logging}
import com.example.Main._
import shapeless._
import spray.client.pipelining._
import spray.http._
import spray.httpx.encoding.{Deflate, Gzip}
import spray.httpx.marshalling.Marshaller
import spray.json._

import scala.concurrent.Future

/**
  * Created by avilevi on 6/13/16.
  */
trait HttpClientWrapper {


  implicit def system: ActorSystem

  implicit def ec = system.dispatcher

  case class BasicHeader(name: String, value: String)

  case class BasicCredential(name: String, value: String)

  sealed abstract class Requestz[T](
                              implicit l: MkFieldLens.Aux[T, Witness.`'headers`.T, Set[BasicHeader]],
                              c: MkFieldLens.Aux[T, Witness.`'credentials`.T, Set[BasicCredential]]
                            ) {
    self: T =>

    def uri: String

    def headers: Set[BasicHeader]

    def credentials: Set[BasicCredential]

    def pipeline: HttpRequest => Future[HttpResponse] = (
      encode(Gzip)
        ~> sendReceive
        ~> decode(Deflate)
      )

    val log: LoggingAdapter = Logging(system, getClass)

    final private val headerLens = lens[T] >> 'headers
    final private val credentialLens = lens[T] >> 'credentials

    private def request: HttpRequest = {
      this match {
        case _: GetRequest with Requestz[_] =>
          log.info(s"Send GET request to $uri")
          Get(uri)
        case post: PostRequest with Requestz[_] =>
          log.debug(s"Send POST request to $uri content: $c")
          post.content.fold(Post(uri)) { content =>
            post.contentType.fold(Post(uri, content)) { t =>
              Post(uri).withEntity(HttpEntity(t, content))
            }
          }
        case put: PutRequest with Requestz[_] =>
          log.info(s"Send Put request to $uri content: $c")
          put.content.fold(Put(uri)) { content =>
            put.contentType.fold(Put(uri, content)) { t =>
              Put(uri).withEntity(HttpEntity(t, content))
            }
          }

        case del: DeleteRequest with Requestz[_] =>
          log.info(s"Send DELETE request to $uri")
          Delete(uri, del.content)
        case r =>
          throw new IllegalArgumentException(s"Unknown request $r")
      }
    }
    def get: Future[HttpResponse] = pipeline {
      val withHeaders = headers.foldLeft(request){case (r,h) => r ~> addHeader(h.name,h.value)}
      val withCredentials = credentials.foldLeft(withHeaders){case (r,credential) => r ~> addCredentials(BasicHttpCredentials(credential.name,credential.value))}
      withCredentials
    }

    def mapTo[R: JsonReader] = {
      get.map {
        case HttpResponse(_, e, _, _) =>
          val x = JsonParser(e.data.asString)
          log.info(s"Raw response ${x.prettyPrint}")
          x.convertTo[R]
      }
    }
    def addHeaders(i: BasicHeader*): T = headerLens.modify(self)(_ ++ i)
    def addCredentialsToRequest(i: BasicCredential*): T = credentialLens.modify(self)(_ ++ i)
  }

  case class GetRequest(val uri: String, headers: Set[BasicHeader] = Set(), credentials: Set[BasicCredential] = Set()) extends Requestz[GetRequest]
  case class PostRequest(val uri: String, content:Option[String] = None, val headers: Set[BasicHeader] = Set(), val credentials: Set[BasicCredential] = Set(), contentType:Option[ContentType] =None) extends Requestz[PostRequest]{
    def addContentType(c:ContentType) = copy(contentType = Option(c))
  }
  case class PutRequest(val uri: String, content:Option[String] = None, val headers: Set[BasicHeader] = Set(), val credentials: Set[BasicCredential] = Set(), contentType:Option[ContentType] =None) extends Requestz[PutRequest]{
    def addContentType(c:ContentType) = copy(contentType = Option(c))
  }

  case class DeleteRequest(val uri: String,  content:Option[String] = None, val headers: Set[BasicHeader] = Set(), val credentials: Set[BasicCredential] = Set()) extends Requestz[DeleteRequest]

}
