name := """spray-wrapper"""

version := "1.0"

scalaVersion := "2.11.2"
resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)

libraryDependencies ++= Seq(
  "io.spray" %% "spray-can" % "1.3.3",
  "io.spray" %% "spray-routing" % "1.3.3",
  "com.typesafe.akka" %% "akka-actor" % "2.3.11",
  "com.chuusai" %% "shapeless" % "2.3.1",
"com.typesafe.akka" %% "akka-testkit" % "2.3.11" % "test",
"org.scalatest" %% "scalatest" % "2.2.4" % "test")
// http://mvnrepository.com/artifact/io.spray/spray-client
libraryDependencies += "io.spray" %% "spray-json" % "1.3.2"
libraryDependencies += "io.spray" %% "spray-client" % "1.3.1"

